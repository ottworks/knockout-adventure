"Knockout Adventure"

Release along with a website and an interpreter.


[Setting up sounds]

A thing has some text called sound. The sound of a thing is usually "silence".

The report listening rule is not listed in the report listening to rules.

Carry out listening to something:
	say "From [the noun] you hear [the sound of the noun]."

Instead of listening to a room:
	if an audible thing can be touched by the player, say "You hear [list of audible things which can be touched by the player].";
	otherwise say "Nothing of note."

Definition: a thing is audible if the sound of it is not "silence".

Before printing the name of something audible while listening to a room:
	say "[sound] from the "
	

[Setting up more helpful directions]

Definition: a direction (called thataway) is viable if the room thataway from the location is a room. 

Instead of going nowhere:
	let count of exits be the number of viable directions;
	if the count of exits is 0, say "You appear to be trapped in here." instead;
	if the count of exits is 1, say "From here, the only way out is [list of viable directions].";
	otherwise say "From here, the viable exits are [list of viable directions]." 



The train station is a room.  

A restless man is in the train station.  "A man restlessly paces back and forth."
The sound of the restless man is "'...they're always departing but they never arrive... and the ones that do arrive, they-they never leave... you never see them go... they're always full... no one ever gets on... but they're always... they're always departing but they never arrive...'".

The courtyard is north of the train station.