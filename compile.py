import subprocess
import pathlib
import shutil
import urllib.request
import json
from dateutil import parser 
from dateutil.tz import gettz 
from datetime import datetime
from datetime import timezone
import time
from subprocess import PIPE


#sudo apt-get install python3-pip
#pip3 install python-dateutil

storyfile = "test.inform/Source/story.ni"
htmout = "test.materials/Release/log.html"
threadid = 23124
encoding = "latin1"

inform = pathlib.Path(storyfile)
backup = pathlib.Path(storyfile + ".bak")
compilefile = pathlib.Path("compiled")
if not backup.exists():
	shutil.copyfile(storyfile, storyfile + ".bak")
if not compilefile.exists():
	subprocess.run(["touch", compilefile])



def find_code(content):
	#remove quotes first
	while True:
		quote = content.find("[quote")
		if quote != -1:
			endquote = content.find("[/quote]")
			content = content[:quote] + content[endquote + 8:]
			#print("removed quote")
		else:
			break
	ret = ""
	while True:
		code = content.find("[code")
		if code != -1:
			codeend = content.find("]", code)
			endcode = content.find("[/code]")
			ret += content[codeend + 1: endcode] + "\n"
			content = content[:code] + content[endcode + 7:]
			#print("found code")
		else:
			break
	if len(ret) > 0:
		return ret
	return None

lastpostmodified = None
def check_thread(page = 1):
	global lastpostmodified
	print("Checking page", page)
	with urllib.request.urlopen("https://api.knockout.chat/thread/" + str(threadid) + "/" + str(page)) as response:
		read = str(response.read().decode("utf-8"))
		thread = json.loads(read)

		if thread['postCount'] <= 20 or page != 1:
			compiledate = inform.stat().st_mtime
			compileparse = datetime.fromtimestamp(compiledate, tz=timezone.utc)
			compilefiledate = compilefile.stat().st_mtime
			compilefileparse = datetime.fromtimestamp(compilefiledate, tz=timezone.utc)
			for post in thread['posts']:
				postdate = post['updatedAt']
				postparse = parser.isoparse(postdate)
				print("Last compiled start:", compilefileparse, "Last compiled finish:", compileparse, "Last modified:", postparse)
				if postparse > compilefileparse or lastpostmodified is None or postparse > lastpostmodified:
					lastpostmodified = postparse
					subprocess.run(["touch", compilefile])
					compile_thread(thread['postCount'])
					break
		else:
			page_count = (postCount - 1) // 20 + 1
			check_thread(page = page_count)


def compile_thread(postCount):
	page_count = (postCount - 1) // 20 + 1
	print("Compiling thread with", page_count, "pages")
	all_code = ""
	for i in range(page_count):
		print("Page", i + 1)
		with urllib.request.urlopen("https://api.knockout.chat/thread/" + str(threadid) + "/" + str(i + 1)) as response:
			thread = json.loads(response.read().decode("utf-8"))
			posts = thread['posts']
			for post in posts:
				code = find_code(post['content'])
				if code is not None:
					all_code += code
	print("Read", len(all_code.split("\n")), "lines of code.")
	all_code = '"Knockout Adventure"\n\nRelease along with a website and an interpreter.\n\n' + all_code
	open(storyfile, "w").write(all_code)
	compile()

def compile():
	if not backup.exists():
		shutil.copyfile(storyfile, storyfile + ".bak")
	fstats = inform.stat()
	bstats = backup.stat()
	if fstats.st_mtime > bstats.st_mtime:
		shutil.copyfile(storyfile, storyfile + ".bak")
	else:
		shutil.copyfile(storyfile + ".bak", storyfile)

	htmlines = []

	def collect_errors():
		lines = []
		result = subprocess.run(["i7", "-r", "test.inform"], stdout=PIPE, stderr=PIPE)
		print(result.stdout)
		print(result.stderr)
		errstr = str(result.stderr.decode(encoding))
		errs = errstr.split(">-->")
		
		for err in errs:
			origerr = err
			err = err.replace(" ", "")
			err = err.replace("\t", "")
			err = err.replace("\n", "")
			err = err.replace("\\n", "")
			candidates = []
			findline = err.find('(sourcetext,line')
			findend = err.find(")", findline)
			if findline != -1 and findend != -1:
				badline = int(err[findline + 16:findend])
				candidates.append(badline)

				while err.find('(sourcetext,line', findline + 1) != -1:
					findline = err.find('(sourcetext,line', findline + 1)
					findend = err.find(")", findline)
					badline = int(err[findline + 16:findend])
					candidates.append(badline)
				candidates.sort()

				lines.append((candidates[-1], origerr))
		return lines

	while True:
		storylines = open(storyfile, "r").read().split("\n")
		if len(htmlines) == 0:
			for storyline in storylines:
				htmlines.append({
					"line": '<span class="line">' + storyline + '</span>\n',
					"children": [],
				})

		errlines = collect_errors()
		if len(errlines) == 0:
			break
		errlines = list(set(errlines))
		errlines.sort()
		print("Lines to remove:", errlines)
		for (errline, err) in reversed(errlines):
			htmlines[errline - 1]["line"] = '<span class="line errline collapsed">' + storylines[errline - 1] + '<span class="err">' + err + '</span></span>\n'
			htmlines[errline - 2]["children"].append(htmlines.pop(errline - 1))
			storylines.pop(errline - 1)

		f = open(storyfile, "w").write("\n".join(storylines))
		print("Removed", len(errlines), "lines of code.")

	errlines = collect_errors()
	print("Done!", errlines)

	subprocess.run(["touch", storyfile + ".bak"])
	html = ""

	def append_children(line):
		html = ""
		html += line["line"]
		for child in line["children"]:
			html += append_children(child)
		return html

	for line in htmlines:
		html += append_children(line)

	open(htmout, "w").write('<!DOCTYPE html><html><head><meta charset="utf-8"><link rel="stylesheet" type="text/css" href="logstyle.css"><script type="text/javascript" src="logscript.js"></script></head><body>' + html + '</body></html>')

while True:
	check_thread()
	time.sleep(60.0)
